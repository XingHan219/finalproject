package jcd.gui;

import java.io.IOException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import static jcd.PropertyType.ADDCLASS_ICON;
import static jcd.PropertyType.ADDCLASS_TOOLTIP;
import static jcd.PropertyType.ADDINTERCAE_TOOLTIP;
import static jcd.PropertyType.ADDINTERFACE_ICON;
import static jcd.PropertyType.REDO_ICON;
import static jcd.PropertyType.REDO_TOOLTIP;
import static jcd.PropertyType.REMOVE_ICON;
import static jcd.PropertyType.REMOVE_TOOLTIP;
import static jcd.PropertyType.RESIZE_ICON;
import static jcd.PropertyType.RESIZE_TOOLTIP;
import static jcd.PropertyType.SELECT_ICON;
import static jcd.PropertyType.SELECT_TOOLTIP;
import static jcd.PropertyType.UNDO_ICON;
import static jcd.PropertyType.UNDO_TOOLTIP;
import static jcd.PropertyType.ZOOMIN_ICON;
import static jcd.PropertyType.ZOOMIN_TOOLTIP;
import static jcd.PropertyType.ZOOMOUT_ICON;
import static jcd.PropertyType.ZOOMOUT_TOOLTIP;
import jcd.controller.CanvasController;
import jcd.controller.PoseEditController;
import jcd.data.DataManager;
import jcd.data.Method;
import static jcd.data.PoseMakerState.SELECTING_SHAPE;
import jcd.data.Var;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_SBU_VIEW = "view_pane_grid_snap";
    static final String CLASS_LABEL = "header_label";
    static final String CLASS_SBU_LABEL = "normal_label";
    static final String CLASS_BUTTON = "top";
    static final String EDIT_COMPONENT = "edit_data";
    static final String CANVAS_PANE = "canvas_pane";
    static final String COMPONENT_PANE = "component_pane";
    static final String SUB_COMPONENT_PANE = "sub_component_pane";
    static final String SUB_SUB_COMPONENT_PANE = "sub_sub_component_pane";
    static final String INSIDE_COMPONENT_PANE = "inside_component_pane";
    static final String SCROLL_BAR = "scroll .increment-button";
    static final String COMPONENT_LABEL = "component_label";
    static final int SACLEFACTOR = 2;
    static final int defaultGridNum = 75;
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    ObservableList<Node> list;
    Group zoomGroup;
    Group contentGroup;
    Scale scaleTransform;
    PoseEditController poseEditController;
    CanvasController canvasController;

    // EDIT TOOL BUTTON
    Button select;
    Button resize;
    Button addClass;
    Button addInterface;
    Button remove;
    Button undo;
    Button redo;
    Button zoomIn;
    Button zoomOut;

    CheckBox grid;
    CheckBox snap;
    VBox subViewToolBar;

    Label ClassName;
    TextField ClassNameTextField;
    HBox ClassNameHB;

    Label PackageName;
    TextField PackageNameTextField;
    HBox PackageNameHB;

    Label parentName;
    ChoiceBox parentBox;
    MenuBar parentMenuBar;
    Menu parentMenu;
    HBox parentNameHB;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    ScrollPane canvasScroll;
    Pane canvas;
    GridPane canvasGrid;
    StackPane canvasStack;

    VBox varPane;
    VBox methodPane;
    Label varLabel;
    Label methodLabel;
    Button addVar;
    Button addMethod;
    Button removeVar;
    Button removeMethod;
    Button addParent;
    Button removeParent;
    Button addParentInterface;
    TextField parent;
    
    TableView<Var> varTable;
    TableView<Method> methodTable;

    VBox componentPane;
    GridPane componentPaneUp;
    VBox componentPaneDown;

    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    Double gridFactor;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        layoutGUI();
        setupHandlers();
    }

    private void layoutGUI() {
        // THIS WILL GO IN THE TOP SIDE OF THE WORKSPACE
        // EDIT TOOLBAR
        HBox editToolBar = gui.getEditToolPane();
        select = gui.initChildButton(editToolBar, SELECT_ICON.toString(), SELECT_TOOLTIP.toString(), false);
        resize = gui.initChildButton(editToolBar, RESIZE_ICON.toString(), RESIZE_TOOLTIP.toString(), false);
        addClass = gui.initChildButton(editToolBar, ADDCLASS_ICON.toString(), ADDCLASS_TOOLTIP.toString(), false);
        addInterface = gui.initChildButton(editToolBar, ADDINTERFACE_ICON.toString(), ADDINTERCAE_TOOLTIP.toString(), false);
        remove = gui.initChildButton(editToolBar, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        undo = gui.initChildButton(editToolBar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), false);
        redo = gui.initChildButton(editToolBar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), false);
        //-------------------------------------------------------------------------------------------------------//
        //VIEW TOOLBAR
        HBox viewToolBar = gui.getViewToolPane();
        zoomIn = gui.initChildButton(viewToolBar, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        zoomOut = gui.initChildButton(viewToolBar, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        subViewToolBar = new VBox();
        //-------------------------------------------------------------------------------------------------------//
        grid = new CheckBox("Grid");
        snap = new CheckBox("Snap");
        subViewToolBar.getChildren().add(grid);
        subViewToolBar.getChildren().add(snap);
        viewToolBar.getChildren().add(subViewToolBar);
        //-------------------------------------------------------------------------------------------------------//
        // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        canvas = new Pane();
        
        canvasGrid = new GridPane();
        canvasStack = new StackPane();
        canvasStack.getChildren().add(canvasGrid);
        canvasStack.getChildren().add(canvas);
        
        canvasScroll = new ScrollPane();
        zoomGroup = new Group();
        contentGroup = new Group();
        zoomGroup.getChildren().add(canvasStack);
        contentGroup.getChildren().add(zoomGroup);
        canvasScroll.setContent(contentGroup);

        scaleTransform = new Scale(SACLEFACTOR,SACLEFACTOR);
        canvas.setStyle("-fx-background-color:#ffffff");
        // need to be change
        canvas.setMinHeight(4000);
        canvas.setMinWidth(4000);
        canvasScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        canvasScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        canvasScroll.setStyle("-fx-font-size: 15px;");

        //right bar
        componentPaneUp = new GridPane();

        ClassName = new Label("Class Name:");
        ClassNameTextField = new TextField();
        ClassNameHB = new HBox();
        ClassNameHB.getChildren().addAll(ClassName, ClassNameTextField);

        PackageName = new Label("Package:");
        PackageNameTextField = new TextField();
        PackageNameHB = new HBox();
        PackageNameHB.getChildren().addAll(PackageName, PackageNameTextField);

        //DROP DOWN LIST FOR CHOOSING PARENT
        /*---------------------------------------------------------------------*/
        parentName = new Label("Parent:");
        
        parentMenuBar = new MenuBar();
        parentMenu = new Menu("Parent");
        parentMenuBar.getMenus().add(parentMenu);
        // COMPONNET GRIDPANE UP
        componentPaneUp.setVgap(40);
        componentPaneUp.setHgap(20);
        componentPaneUp.add(ClassName, 0, 0);
        componentPaneUp.add(ClassNameTextField, 1, 0);

        componentPaneUp.add(PackageName, 0, 1);
        componentPaneUp.add(PackageNameTextField, 1, 1);

        componentPaneUp.add(parentName, 0, 2);
        componentPaneUp.add(parentMenuBar, 1, 2);
        
        /*---------------------------------------------------------------------*/

        GridPane varGP = new GridPane();
        varGP.setVgap(10);
        varLabel = new Label("Variables:");
        varGP.add(varLabel, 0, 0);
        // ADD&REMOVE BUTTON LOCATION
        addVar = new Button("Add");
        removeVar = new Button("Remove");
        varGP.add(addVar, 1, 0);
        varGP.add(removeVar, 2, 0);
        // TABLEVIEW OF VAR 
        varTable = new TableView();
        varTable.setEditable(true);
        // ADD BUTTON ACTION
        addVar.setOnAction(e -> {
            poseEditController.addVar();

        });
        // REMOVE BUTTON ACTION
        removeVar.setOnAction(e -> {
            ObservableList<Var> selectedVar;
            selectedVar = varTable.getSelectionModel().getSelectedItems();
            poseEditController.removeVar(selectedVar);
        });

        // EDIT VAR NAME ON TABLEVEIW
        TableColumn<Var, String> varNameCol = new TableColumn<>("Name");
        varNameCol.setMinWidth(100);
        varNameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));

        varNameCol.setCellFactory(TextFieldTableCell.<Var>forTableColumn());
        varNameCol.setOnEditCommit(
                (CellEditEvent<Var, String> t) -> {
                    ((Var) t.getTableView().getItems().get(t.getTablePosition().getRow())).setName(t.getNewValue());
                    //poseEditController.setVarName(SelectedVar,t.getNewValue());

                    poseEditController.updateVarTable(varTable.getItems());
                });
        // EDIT VAR TYPE 
        TableColumn<Var, String> typeCol = new TableColumn<>("Type");
        typeCol.setMinWidth(100);
        typeCol.setCellValueFactory(new PropertyValueFactory<>("Type"));

        typeCol.setCellFactory(TextFieldTableCell.<Var>forTableColumn());
        typeCol.setOnEditCommit(
                (CellEditEvent<Var, String> t) -> {
                    ((Var) t.getTableView().getItems().get(t.getTablePosition().getRow())).setType(t.getNewValue());
                    poseEditController.checkType(t.getNewValue());
                    poseEditController.updateVarTable(varTable.getItems());
                });

        // var isStatic table column
        TableColumn<Var, Boolean> isStaticCol = new TableColumn<>("isStatic");
        isStaticCol.setMinWidth(100);
        isStaticCol.setCellValueFactory(new PropertyValueFactory<>("isStatic"));
        isStaticCol.setCellFactory(column -> {
            CheckBoxTableCell<Var, Boolean> cell = new CheckBoxTableCell<>(index -> {
                
                BooleanProperty selected = new SimpleBooleanProperty(varTable.getItems().get(index).getIsStatic());
                
                selected.addListener((obs, wasActive, nowActive) -> {
                    Var var = varTable.getItems().get(index);
                    var.setIsStatic(nowActive);
                });
                return selected;
            });
            return cell;
        });
        

        // var access table column
        TableColumn<Var, String> accessCol = new TableColumn<>("access");
        accessCol.setMinWidth(100);
        accessCol.setCellValueFactory(new PropertyValueFactory<>("access"));
        accessCol.setCellFactory(TextFieldTableCell.<Var>forTableColumn());
        accessCol.setOnEditCommit(
                (CellEditEvent<Var, String> t) -> {
                    ((Var) t.getTableView().getItems().get(t.getTablePosition().getRow())).setAccess(t.getNewValue());
                    poseEditController.updateVarTable(varTable.getItems());
                });
        // ADD ALL DATA TO TABLE
        varTable.getColumns().addAll(varNameCol, typeCol, isStaticCol, accessCol);
        varPane = new VBox();
        varPane.getChildren().addAll(varGP, varTable);
        /*---------------------------------------------------------------------*/
        /*---------------------------------------------------------------------*/

        GridPane methodGP = new GridPane();

        methodGP.setVgap(10);
        methodLabel = new Label("Methods:");
        methodGP.add(methodLabel, 0, 0);
        // check
        addMethod = new Button("Add");
        removeMethod = new Button("Remove");
        methodGP.add(addMethod, 1, 0);
        methodGP.add(removeMethod, 2, 0);

        methodTable = new TableView();
        methodTable.setEditable(true);
        
        addMethod.setOnAction(e -> {
            poseEditController.addMethod();
            
        });
        // REMOVE BUTTON ACTION
        removeMethod.setOnAction(e -> {
            ObservableList<Method> selectedMethod;
            selectedMethod = methodTable.getSelectionModel().getSelectedItems();
            poseEditController.removeMethod(selectedMethod);
        });
        
        TableColumn<Method, String> M_NameCol = new TableColumn<>("Name");
        M_NameCol.setMinWidth(100);
        M_NameCol.setCellValueFactory(new PropertyValueFactory<>("Name"));
        M_NameCol.setCellFactory(TextFieldTableCell.<Method>forTableColumn());
        M_NameCol.setOnEditCommit(
                (CellEditEvent<Method, String> t) -> {
                    ((Method) t.getTableView().getItems().get(t.getTablePosition().getRow())).setName(t.getNewValue());
                    poseEditController.updateMethodTable(methodTable.getItems());
                });
        
        TableColumn<Method, String> M_ReturnCol = new TableColumn<>("ReturnType");
        M_ReturnCol.setMinWidth(100);
        M_ReturnCol.setCellValueFactory(new PropertyValueFactory<>("ReturnType"));
        M_ReturnCol.setCellFactory(TextFieldTableCell.<Method>forTableColumn());
        M_ReturnCol.setOnEditCommit(
                (CellEditEvent<Method, String> t) -> {
                    ((Method) t.getTableView().getItems().get(t.getTablePosition().getRow())).setReturnType(t.getNewValue());
                    poseEditController.checkTypeForMethod(t.getNewValue());
                    poseEditController.updateMethodTable(methodTable.getItems());
                });
        
        TableColumn<Method, Boolean> M_isStaticCol = new TableColumn<>("isStatic");
        M_isStaticCol.setMinWidth(100);
        M_isStaticCol.setCellValueFactory(new PropertyValueFactory<>("isStatic"));
        M_isStaticCol.setCellFactory(column -> {
            CheckBoxTableCell<Method, Boolean> cell = new CheckBoxTableCell<>(index -> {
                
                BooleanProperty selected = new SimpleBooleanProperty(methodTable.getItems().get(index).getIsStatic());
                
                selected.addListener((obs, wasActive, nowActive) -> {
                    Method newMethod = methodTable.getItems().get(index);
                    newMethod.setIsStatic(nowActive);
                });
                return selected;
            });
            return cell;
        });
        
        TableColumn<Method, Boolean> M_isAbstractCol = new TableColumn<>("isAbstract");
        M_isAbstractCol.setMinWidth(100);
        M_isAbstractCol.setCellValueFactory(new PropertyValueFactory<>("isAbstract"));
        M_isAbstractCol.setCellFactory(column -> {
            CheckBoxTableCell<Method, Boolean> cell = new CheckBoxTableCell<>(index -> {
                
                BooleanProperty selected = new SimpleBooleanProperty(methodTable.getItems().get(index).getIsAbstract());
                
                selected.addListener((obs, wasActive, nowActive) -> {
                    Method newMethod = methodTable.getItems().get(index);
                    newMethod.setIsAbstract(nowActive);
                });
                return selected;
            });
            return cell;
        });
        
        TableColumn<Method, String> M_AccessCol = new TableColumn<>("Access");
        M_AccessCol.setMinWidth(100);
        M_AccessCol.setCellValueFactory(new PropertyValueFactory<>("Access"));
        M_AccessCol.setCellFactory(TextFieldTableCell.<Method>forTableColumn());
        M_AccessCol.setOnEditCommit(
                (CellEditEvent<Method, String> t) -> {
                    ((Method) t.getTableView().getItems().get(t.getTablePosition().getRow())).setAccess(t.getNewValue());
                    poseEditController.updateMethodTable(methodTable.getItems());
                });
        
        
        TableColumn<Method, String> M_ArgNameCol_1 = new TableColumn<>("Argname");
        M_ArgNameCol_1.setMinWidth(100);
        M_ArgNameCol_1.setCellValueFactory(new PropertyValueFactory<>("argName"));
        M_ArgNameCol_1.setCellFactory(TextFieldTableCell.<Method>forTableColumn());
        M_ArgNameCol_1.setOnEditCommit(
                (CellEditEvent<Method, String> t) -> {
                    ((Method) t.getTableView().getItems().get(t.getTablePosition().getRow())).setArgName(t.getNewValue());
                    poseEditController.updateMethodTable(methodTable.getItems());
                });
        
        
        TableColumn<Method, String> M_ArgTypeCol_1 = new TableColumn<>("Argtype");
        M_ArgTypeCol_1.setMinWidth(100);
        M_ArgTypeCol_1.setCellValueFactory(new PropertyValueFactory<>("argType"));
        M_ArgTypeCol_1.setCellFactory(TextFieldTableCell.<Method>forTableColumn());
        M_ArgTypeCol_1.setOnEditCommit(
                (CellEditEvent<Method, String> t) -> {
                    ((Method) t.getTableView().getItems().get(t.getTablePosition().getRow())).setArgType(t.getNewValue());
                    poseEditController.checkTypeForMethod(t.getNewValue());
                    poseEditController.updateMethodTable(methodTable.getItems());
                });

        methodTable.getColumns().addAll(M_NameCol, M_ReturnCol, M_isStaticCol, M_isAbstractCol, M_AccessCol, M_ArgNameCol_1, M_ArgTypeCol_1);
        methodPane = new VBox();
        methodPane.getChildren().addAll(methodGP, methodTable);
        /*---------------------------------------------------------------------*/
        /*---------------------------------------------------------------------*/
        componentPaneDown = new VBox();
        componentPaneDown.getChildren().addAll(varPane, methodPane);
        /*---------------------------------------------------------------------*/
        /*---------------------------------------------------------------------*/
        componentPane = new VBox();
        componentPane.getChildren().add(componentPaneUp);
        componentPane.getChildren().add(componentPaneDown);

        /*---------------------------------------------------------------------*/
        // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
        DataManager data = (DataManager) app.getDataComponent();
        data.setShapes((ObservableList<Node>) canvas.getChildren());
        list = data.getShapes();
        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setRight(componentPane);
        ((BorderPane) workspace).setCenter(canvasScroll);
        /*---------------------------------------------------------------------*/
    }

    /**
     * GET CANVAS FOR SNAPSHOT
     *
     * @return
     */
    @Override
    public Pane getSnapshotPane() {
        return canvas;
    }

    public String getClassName() {
        return ClassName.getText();
    }

    public void SetClassName(String name) {
        ClassName.setText(name);
    }

    public void setPackageName(String name) {
        PackageName.setText(name);
    }

    private void setupHandlers() {

        DataManager dataManager = (DataManager) app.getDataComponent();

        PackageNameTextField.textProperty().addListener(e -> {
            if (dataManager.isInState(SELECTING_SHAPE)) {
                String packageName = PackageNameTextField.getText();
                if (dataManager.checkNewPacageName(packageName) == false) {
                    dataManager.setSelectedPackageName(packageName);
                }
            }
        });

        ClassNameTextField.textProperty().addListener(e -> {
            if (dataManager.isInState(SELECTING_SHAPE)) {
                String className = ClassNameTextField.getText();
                if (dataManager.checkNewClassName(className) == false) {
                    dataManager.setSelectedClassName(className);
                }
            }
        });

        // MAKE THE EDIT CONTROLLER
        poseEditController = new PoseEditController(app);
        // NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        select.setOnAction(e -> {
            
            poseEditController.processSelectSelectionTool();
            for (Node selected : list) {
                selected.setOnMousePressed(e1 -> {
                    Object topOne = e1.getSource();
                    if (topOne instanceof Node) {
                        Node top = (Node) topOne;
                        canvasController.processCanvasMousePress(top, (int) e1.getX(), (int) e1.getY());
                    }
                });
            }
        });

        resize.setOnAction(e -> {

            poseEditController.SelectedToResize();
        });
        addClass.setOnAction(e -> {
            poseEditController.addClass();
        });
        addInterface.setOnAction(e -> {
            poseEditController.addInterface();
        });

        remove.setOnAction(e -> {
            poseEditController.processRemoveSelectedShape();
        });
        undo.setOnAction(e -> {
            if(dataManager.getUndoList().size()>0){
                //System.out.println("undo times "+dataManager.getUndoList().size());
                dataManager.changeShapes(dataManager.getUndoList().pop());
                loadSelectedShapeSettings(dataManager.getSelectedShape());
            }            

        });

        redo.setOnAction(e -> {
            if(dataManager.getRedoList().size()>0){
                //System.out.println("undo times "+dataManager.getUndoList().size());
                dataManager.redoChange(dataManager.getRedoList().pop());
                loadSelectedShapeSettings(dataManager.getSelectedShape());
            } 

        });
        //-------------------------------------------------------------------------------------------------------//

        zoomIn.setOnAction(e -> {
//            scaleTransform = new Scale(SACLEFACTOR, SACLEFACTOR);
//            zoomGroup.getTransforms().add(scaleTransform);
            canvasStack.setScaleX(canvasStack.getScaleX() * SACLEFACTOR);
            canvasStack.setScaleY(canvasStack.getScaleY() * SACLEFACTOR);
//            if (grid.isSelected()) {
//                setOffGrid();
//                setOnGrid(1/SACLEFACTOR);
//                canvas.setStyle("-fx-background-color: transparent");
//            }
            
            
        });
        zoomOut.setOnAction(e -> {
//            scaleTransform = new Scale(1/SACLEFACTOR, 1/SACLEFACTOR);
//            zoomGroup.getTransforms().add(scaleTransform);

            canvasStack.setScaleX(canvasStack.getScaleX() / SACLEFACTOR);
            canvasStack.setScaleY(canvasStack.getScaleY() / SACLEFACTOR);
//            if (grid.isSelected()) {
//                setOffGrid();
//                setOnGrid(SACLEFACTOR);
//                canvas.setStyle("-fx-background-color: transparent");
//            }

        });
        grid.setOnAction(e -> {
            if (grid.isSelected()) {
                setOnGrid();
                canvas.setStyle("-fx-background-color: transparent");
            } else {
                setOffGrid();
                canvas.setStyle("-fx-background-color: #ffffff");
            }
        });
        snap.setOnAction(e -> {
            if (snap.isSelected()) {
                poseEditController.Snapping(gridFactor);
            }
            else{
                poseEditController.SnappingOff();
            }
        });
        //-------------------------------------------------------------------------------------------------------//

        
        //-------------------------------------------------------------------------------------------------------//
        // MAKE THE CANVAS CONTROLLER	
        canvasController = new CanvasController(app);
              
        canvas.setOnMouseClicked(e -> {
            canvasController.processCanvasMouseClicked((int) e.getX(), (int) e.getY());
        });

        canvas.setOnMouseReleased(e -> {
            canvasController.processCanvasMouseRelease((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseDragged(e -> {
            canvasController.processCanvasMouseDragged((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseExited(e -> {
            canvasController.processCanvasMouseExited((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseMoved(e -> {
            
            canvasController.processCanvasMouseMoved((int) e.getX(), (int) e.getY());
        });
    }

    public void setImage(ButtonBase button, String fileName) {
        // LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + fileName;
        Image buttonImage = new Image(imagePath);

        // SET THE IMAGE IN THE BUTTON
        button.setGraphic(new ImageView(buttonImage));
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {

        DataManager dataManager = (DataManager) app.getDataComponent();

    }

    public void loadSelectedShapeSettings(Node shape) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.getSelectedClass() != null) {
            if (!dataManager.getSelectedClass().isAggregate()) {
                PackageNameTextField.setText(dataManager.getSelectedPackageName());
                ClassNameTextField.setText(dataManager.getSelectedClassName());
                varTable.setItems(dataManager.getSelectedVarList());
                methodTable.setItems(dataManager.getSelectedMethodList());
                dataManager.setMenuItem(parentMenu);
                menuCheck();
            }
        }
        else{
            PackageNameTextField.setText(null);
            ClassNameTextField.setText(null);
            ClassNameTextField.setPromptText("Dummy");
            PackageNameTextField.setPromptText("Dummy.Parent");
            varTable.setItems(null);
            methodTable.setItems(null);
        }

    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        canvas.getStyleClass().add(CANVAS_PANE);
        componentPane.getStyleClass().add(COMPONENT_PANE);
        componentPaneUp.getStyleClass().add(SUB_COMPONENT_PANE);
        componentPaneDown.getStyleClass().add(SUB_COMPONENT_PANE);
        varPane.getStyleClass().add(SUB_SUB_COMPONENT_PANE);
        methodPane.getStyleClass().add(SUB_SUB_COMPONENT_PANE);
        varTable.getStyleClass().add(INSIDE_COMPONENT_PANE);
        methodTable.getStyleClass().add(INSIDE_COMPONENT_PANE);

        addVar.getStyleClass().add(EDIT_COMPONENT);
        addMethod.getStyleClass().add(EDIT_COMPONENT);
        removeVar.getStyleClass().add(EDIT_COMPONENT);
        removeMethod.getStyleClass().add(EDIT_COMPONENT);
        addClass.getStyleClass().add(EDIT_COMPONENT);
        addInterface.getStyleClass().add(EDIT_COMPONENT);
        
        ClassName.getStyleClass().add(CLASS_LABEL);
        PackageName.getStyleClass().add(CLASS_SBU_LABEL);
        parentName.getStyleClass().add(CLASS_SBU_LABEL);
        varLabel.getStyleClass().add(CLASS_SBU_LABEL);
        methodLabel.getStyleClass().add(CLASS_SBU_LABEL);
        // file toolbar
        select.getStyleClass().add(CLASS_BUTTON);
        resize.getStyleClass().add(CLASS_BUTTON);
        addClass.getStyleClass().add(CLASS_BUTTON);
        addInterface.getStyleClass().add(CLASS_BUTTON);
        remove.getStyleClass().add(CLASS_BUTTON);
        undo.getStyleClass().add(CLASS_BUTTON);
        redo.getStyleClass().add(CLASS_BUTTON);
        zoomIn.getStyleClass().add(CLASS_BUTTON);
        zoomOut.getStyleClass().add(CLASS_BUTTON);
        subViewToolBar.getStyleClass().add(CLASS_SBU_VIEW);

    }

    public void setOnGrid(int SACLEFACTOR) {
        int num = defaultGridNum * SACLEFACTOR ;

        for (int i = 0; i < num; i++) {
            RowConstraints row = new RowConstraints();
            row.setPercentHeight(100.0 / num);
            canvasGrid.getRowConstraints().add(row);
        }
        for (int i = 0; i < num; i++) {
            ColumnConstraints col = new ColumnConstraints();
            col.setPercentWidth(100.0 / num);
            canvasGrid.getColumnConstraints().add(col);
            gridFactor = canvas.getHeight() / num;
        }

        canvasGrid.setGridLinesVisible(true);
    }
    
    public void setOnGrid() {
        int num = defaultGridNum;

        for (int i = 0; i < num; i++) {
            RowConstraints row = new RowConstraints();
            row.setPercentHeight(100.0 / num);
            canvasGrid.getRowConstraints().add(row);
        }
        for (int i = 0; i < num; i++) {
            ColumnConstraints col = new ColumnConstraints();
            col.setPercentWidth(100.0 / num);
            canvasGrid.getColumnConstraints().add(col);
            gridFactor = canvas.getHeight() / num;
        }

        canvasGrid.setGridLinesVisible(true);
    }

    public void setOffGrid() {
        canvasGrid.getRowConstraints().clear();
        canvasGrid.getColumnConstraints().clear();
        canvasGrid.setGridLinesVisible(false);
    }
    
    public void menuCheck(){
        DataManager dataManager = (DataManager) app.getDataComponent();
        for(int i=0; i<parentMenu.getItems().size()-1; i++){
        CheckMenuItem temp = (CheckMenuItem) parentMenu.getItems().get(i);
        temp.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue ov, Boolean old_val, Boolean new_val) {
                    if(new_val){
                        if(dataManager.getSelectedClass()!=dataManager.getNameOfClass(temp.getText())){
                            dataManager.addParentClass(dataManager.getNameOfClass(temp.getText()));
                        }
                        else{
                            temp.setSelected(false);
                        }
                        
                        //System.out.println("add parent class "+temp.getText());
                    }
                    else{
                        dataManager.removeSelectedLine(dataManager.getNameOfClass(temp.getText()));
                        //System.out.println("remove parent class "+temp.getText());
                    }
                }
            });
    
        } 
        MenuItem Add = (MenuItem) parentMenu.getItems().get(parentMenu.getItems().size()-1);
        Add.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                dataManager.addExternalClass();
                
            }
        });
    }
}
