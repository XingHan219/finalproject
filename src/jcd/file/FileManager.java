package jcd.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.ClassUnit;
import jcd.data.DataManager;
import jcd.data.LineUnit;
import jcd.data.Method;
import jcd.data.Var;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import static saf.settings.AppStartupConstants.PATH_TEMP;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    // FOR JSON LOADING
    static final String JSON_CLASS = "JSON_CLASS_DATA";
    static final String JSON_CLASS_OBJECT = "JSON_CLASS_OBJECT_LIST";
    static final String JSON_CLASS_LINE_OBJECT = "JSON_CLASS_LINE_LIST";

    static final String JSON_CLASS_TYPE = "CALSS_TYPE";

    static final String JSON_CLASS_EXTERNAL = "EXTERNAL";
    static final String JSON_CLASS_INTERFACE = "INTERFACE";
    static final String JSON_CLASS_AGGREGATE = "AGGREGATE";
    static final String JSON_CLASS_INHERITS = "INHERITS";
    static final String JSON_CLASS_USE = "USE";

    static final String JSON_CLASS_NAME = "CLASS_NAME";
    static final String JSON_CLASS_PACKAGE = "CLASS_PACKAGE";

    static final String JSON_CLASS_LOCATION_X = "CLASS_LOCATION_X";
    static final String JSON_CLASS_LOCATION_Y = "CLASS_LOCATION_Y";
    static final String JSON_CLASS_WIDTH = "CLASS_WIDTH";
    static final String JSON_CLASS_HEIGHT = "CALSS_HEIGHT";

    static final String JSON_CLASS_PARENT = "CLASS_PARENT";
    static final String JSON_CLASS_LINE = "CLASS_LINE";

    static final String JSON_CLASS_START = "LINE_START_NAME";
    static final String JSON_CLASS_END = "LINE_END_NAME";
    static final String JSON_LINE_TYPE = "LINE_END_TYPE";

    static final String JSON_CLASS_LINE_START_X = "START_X";
    static final String JSON_CLASS_LINE_START_Y = "START_Y";
    static final String JSON_CLASS_LINE_END_X = "END_X";
    static final String JSON_CLASS_LINE_END_Y = "END_Y";

    static final String JSON_VAR = "VAR_LIST";
    static final String JSON_VAR_NAME = "VAR_NAME";
    static final String JSON_VAR_TYPE = "VAR_TYEP";
    static final String JSON_VAR_STATIC = "VAR_ISSTATIC";
    static final String JSON_VAR_ACCESS = "VAR_ACCESS";

    static final String JSON_METHOD = "METHOD_LIST";
    static final String JSON_METHOD_NAME = "METHOD_NAME";
    static final String JSON_METHOD_RETURN_TYPE = "METHOD_RETURN_TYPE";
    static final String JSON_METHOD_STATIC = "METHOD_STATIC";
    static final String JSON_METHOD_ABSTRACT = "METHOD_ABSTRACT";
    static final String JSON_METHOD_ACCESS = "METHOD_ACCESS";
    static final String JSON_METHOD_ARG_NAME = "METHOD_ARGUMENT_NAME";
    static final String JSON_METHOD_ARG_TYPE = "METHOD_ARGUMNET_TYPE";

    static final String JAVA_FOUR_SPACES = "    ";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        DataManager dataManager = (DataManager) data;

        ObservableList<Node> classUnitList = dataManager.getShapes();

        JsonArrayBuilder class_list = Json.createArrayBuilder();
        JsonArrayBuilder class_line_list = Json.createArrayBuilder();
        for (Node node : classUnitList) {
            // bu zhi dao zen me xie
            if (node instanceof ClassUnit) {
                ClassUnit classBox = (ClassUnit) node;
                class_list.add(makeClassUnit(classBox));
            } else if (node instanceof LineUnit) {
                LineUnit classLine = (LineUnit) node;
                class_line_list.add(makeLineJSON(classLine));
            }

        }
        JsonArray class_listJSON = class_list.build();
        JsonArray line_listJSON = class_line_list.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_CLASS_OBJECT, class_listJSON)
                .add(JSON_CLASS_LINE_OBJECT, line_listJSON)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    public JsonObject makeClassUnit(ClassUnit unit) {
        Boolean external = unit.isExternal();
        Boolean isInterface = unit.isInterface();
        Boolean isAggregate = unit.isAggregate();
        Boolean isInherits = unit.getIsInherits();
        Boolean isUse = unit.isUse();

        String className = unit.getClassName();
        String packageName = unit.getPackageName();
        String x = Double.toString(unit.getLayoutX());
        String y = Double.toString(unit.getLayoutY());
        String width = Double.toString(unit.getPrefWidth());
        String height = Double.toString(unit.getPrefHeight());

        ObservableList<ClassUnit> parentList = unit.getParentList();

        ObservableList<Var> varList = unit.getNewVarList();

        ObservableList<Method> methodList = unit.getNewMethodList();

        //build json class property list
        JsonObject classUnit = Json.createObjectBuilder()
                .add(JSON_CLASS_EXTERNAL, external)
                .add(JSON_CLASS_INTERFACE, isInterface)
                .add(JSON_CLASS_AGGREGATE, isAggregate)
                .add(JSON_CLASS_INHERITS, isInherits)
                .add(JSON_CLASS_USE, isUse)
                .add(JSON_CLASS_NAME, className)
                .add(JSON_CLASS_PACKAGE, packageName)
                .add(JSON_CLASS_LOCATION_X, x)
                .add(JSON_CLASS_LOCATION_Y, y)
                .add(JSON_CLASS_WIDTH, width)
                .add(JSON_CLASS_HEIGHT, height)
                .add(JSON_CLASS_PARENT, makeParentJSON(parentList))
                .add(JSON_VAR, makeVarJSON(varList))
                .add(JSON_METHOD, makeMethodJSON(methodList))
                .build();

        return classUnit;

    }

    public JsonObject makeParentJSON(ObservableList<ClassUnit> parentList) {
        System.out.println(parentList.size());
        JsonArrayBuilder parent_array = Json.createArrayBuilder();
        for (ClassUnit unit : parentList) {
            Boolean external = unit.isExternal();
            Boolean isInterface = unit.isInterface();
            Boolean isAggregate = unit.isAggregate();
            Boolean isInherits = unit.getIsInherits();
            Boolean isUse = unit.isUse();
            String className = unit.getClassName();
            System.out.println("times");
            JsonObject parentUnit = Json.createObjectBuilder()
                    .add(JSON_CLASS_NAME, className)
                    .add(JSON_CLASS_EXTERNAL, external)
                    .add(JSON_CLASS_INTERFACE, isInterface)
                    .add(JSON_CLASS_AGGREGATE, isAggregate)
                    .add(JSON_CLASS_INHERITS, isInherits)
                    .add(JSON_CLASS_USE, isUse)
                    .build();
            parent_array.add(parentUnit);
        }
        JsonArray parentListJSON = parent_array.build();
        JsonObject parentJSON = Json.createObjectBuilder()
                .add(JSON_CLASS_PARENT, parentListJSON)
                .build();
        return parentJSON;

    }

    public JsonObject makeLineJSON(LineUnit line) {
//        JsonArrayBuilder parent_array = Json.createArrayBuilder();
//        for (LineUnit line : lineList) {
        double startX = line.getStartX();
        double startY = line.getStartY();
        double endX = line.getEndX();
        double endY = line.getEndY();
        ClassUnit start = line.getStart();
        ClassUnit end = line.getEnd();
        ClassUnit unit = line.getConnectorType();
        Boolean isAggregate = unit.isAggregate();
        Boolean isInherits = unit.getIsInherits();
        Boolean isUse = unit.isUse();

        JsonObject LineUnitJSON = Json.createObjectBuilder()
                .add(JSON_CLASS_LINE_START_X, startX)
                .add(JSON_CLASS_LINE_START_Y, startY)
                .add(JSON_CLASS_LINE_END_X, endX)
                .add(JSON_CLASS_LINE_END_Y, endY)
                .add(JSON_CLASS_START, start.getClassName())
                .add(JSON_CLASS_END, end.getClassName())
                .add(JSON_LINE_TYPE, unit.getClassName())
                .add(JSON_CLASS_AGGREGATE, isAggregate)
                .add(JSON_CLASS_INHERITS, isInherits)
                .add(JSON_CLASS_USE, isUse)
                .build();
//            parent_array.add(LineUnit);
//        }
//        JsonArray parentListJSON = parent_array.build();
//        return parentListJSON;
        return LineUnitJSON;

    }

    public JsonObject makeVarJSON(ObservableList<Var> varList) {

        JsonArrayBuilder var_array = Json.createArrayBuilder();
        for (Var var : varList) {
            String name = var.getName();
            String type = var.getType();
            Boolean v_static = var.getIsStatic();
            String v_access = var.getAccess();

            JsonObject varJson = Json.createObjectBuilder()
                    .add(JSON_VAR_NAME, name)
                    .add(JSON_VAR_TYPE, type)
                    .add(JSON_VAR_STATIC, v_static)
                    .add(JSON_VAR_ACCESS, v_access)
                    .build();
            var_array.add(varJson);
        }
        JsonArray varListJSON = var_array.build();
        JsonObject varJSON = Json.createObjectBuilder()
                .add(JSON_VAR, varListJSON)
                .build();
        return varJSON;
    }

    public JsonObject makeMethodJSON(ObservableList<Method> methodListt) {
        JsonArrayBuilder method_array = Json.createArrayBuilder();
        for (Method method : methodListt) {
            String name = method.getName();
            String return_type = method.getReturnType();
            Boolean m_static = method.getIsStatic();
            Boolean m_abstract = method.getIsAbstract();
            String m_access = method.getAccess();
            String m_arg_name = method.getArgName();
            String m_arg_type = method.getArgType();
            JsonObject methodJson = Json.createObjectBuilder()
                    .add(JSON_METHOD_NAME, name)
                    .add(JSON_METHOD_RETURN_TYPE, return_type)
                    .add(JSON_METHOD_STATIC, m_static)
                    .add(JSON_METHOD_ABSTRACT, m_abstract)
                    .add(JSON_METHOD_ACCESS, m_access)
                    .add(JSON_METHOD_ARG_NAME, m_arg_name)
                    .add(JSON_METHOD_ARG_TYPE, m_arg_type)
                    .build();

            method_array.add(methodJson);
        }
        JsonArray methodListJSON = method_array.build();
        JsonObject methodJSON = Json.createObjectBuilder()
                .add(JSON_METHOD, methodListJSON)
                .build();
        return methodJSON;

    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        DataManager dataManager = (DataManager) data;
        dataManager.reset();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // AND NOW LOAD ALL THE SHAPES
        JsonArray ClassArray = json.getJsonArray(JSON_CLASS_OBJECT);
        JsonArray LineArray = json.getJsonArray(JSON_CLASS_LINE_OBJECT);
        for (int i = 0; i < ClassArray.size(); i++) {
            JsonObject ClassJSON = ClassArray.getJsonObject(i);
            ClassUnit classUnit = new ClassUnit();
            classUnit.startNewClassUnit(0, 0);
            loadClassObject(ClassJSON, classUnit);
            JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
            JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
            saveParentName(parentListJSON, classUnit, dataManager);
            dataManager.addShape(classUnit);

        }
        //UPDATE CLASS PARENT 
        for (int j = 0; j < dataManager.getShapes().size(); j++) {
//            System.out.println("load parent" + j);
            Node temp = dataManager.getShapes().get(j);
            ClassUnit t = (ClassUnit) temp;
                
            loadClassParent(t, dataManager);
            
        }

        for (int k = 0; k < LineArray.size(); k++) {
//            System.out.println("load line" + k);
            JsonObject LineJSON = LineArray.getJsonObject(k);

            LineUnit line = loadClassLine(LineJSON, dataManager);

            dataManager.addShape(line);
            dataManager.addShape(line.addConnectorSign(line.getConnectorType()));
        }

    }

    public void loadClassObject(JsonObject ClassJSON, ClassUnit t) {

        Boolean isExternal = ClassJSON.getBoolean(JSON_CLASS_EXTERNAL);
        Boolean isInterface = ClassJSON.getBoolean(JSON_CLASS_INTERFACE);
        Boolean isAggregate = ClassJSON.getBoolean(JSON_CLASS_AGGREGATE);
        Boolean isInherits = ClassJSON.getBoolean(JSON_CLASS_INHERITS);
        Boolean isUse = ClassJSON.getBoolean(JSON_CLASS_USE);

        String className = ClassJSON.getString(JSON_CLASS_NAME);
        String packageName = ClassJSON.getString(JSON_CLASS_PACKAGE);
        double x = Double.parseDouble(ClassJSON.getString(JSON_CLASS_LOCATION_X));
        double y = Double.parseDouble(ClassJSON.getString(JSON_CLASS_LOCATION_Y));
        double width = Double.parseDouble(ClassJSON.getString(JSON_CLASS_WIDTH));
        double height = Double.parseDouble(ClassJSON.getString(JSON_CLASS_HEIGHT));

        t.loadClassUnit(isAggregate, isInterface, isInherits, isUse, isExternal,
                x, y, width, height, className, packageName);
        JsonObject VarJSON = ClassJSON.getJsonObject(JSON_VAR);
        JsonArray varListJSON = VarJSON.getJsonArray(JSON_VAR);
        loadClassVar(varListJSON, t);
        JsonObject methodJSON = ClassJSON.getJsonObject(JSON_METHOD);
        JsonArray methodListJSON = methodJSON.getJsonArray(JSON_METHOD);
        loadClassMethod(methodListJSON, t);

    }
    
    public void saveParentName(JsonArray ParentJSON, ClassUnit t, DataManager dataManager) {

        for (int i = 0; i < ParentJSON.size(); i++) {
            String name = ParentJSON.getJsonObject(i).getString(JSON_CLASS_NAME);
            t.addParentNameToList(name);

        }
        System.out.println(t.getParentList().size());
    }

    public void loadClassParent(ClassUnit t, DataManager dataManager) {

        for (int i = 0; i < t.getParentNameList().size(); i++) {
            String name = t.getParentNameList().get(i);
            t.addParentList(dataManager.getNameOfClass(name));

        }
        System.out.println(t.getParentList().size());
    }

    public void loadClassVar(JsonArray varListJSON, ClassUnit t) {

        for (int i = 0; i < varListJSON.size(); i++) {
            String name = varListJSON.getJsonObject(i).getString(JSON_VAR_NAME);
            String type = varListJSON.getJsonObject(i).getString(JSON_VAR_TYPE);
            Boolean isStatic = varListJSON.getJsonObject(i).getBoolean(JSON_VAR_STATIC);
            String Access = varListJSON.getJsonObject(i).getString(JSON_VAR_ACCESS);
            Var var = new Var(name, type, isStatic, Access);
            t.addNewVar(var);
        }

    }

    public void loadClassMethod(JsonArray methodListJSON, ClassUnit t) {

        for (int i = 0; i < methodListJSON.size(); i++) {
            String name = methodListJSON.getJsonObject(i).getString(JSON_METHOD_NAME);
            String type = methodListJSON.getJsonObject(i).getString(JSON_METHOD_RETURN_TYPE);
            Boolean isStatic = methodListJSON.getJsonObject(i).getBoolean(JSON_METHOD_STATIC);
            Boolean m_abstract = methodListJSON.getJsonObject(i).getBoolean(JSON_METHOD_ABSTRACT);
            String Access = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ACCESS);
            String m_arg_name = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ARG_NAME);
            String m_arg_type = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ARG_TYPE);
            Method defaultMethod = new Method(name, type, isStatic, m_abstract, Access, m_arg_name, m_arg_type);
            t.addNewMethod(defaultMethod);
        }

    }

    public LineUnit loadClassLine(JsonObject LineJSON, DataManager dataManager) {

        ClassUnit start = dataManager.getNameOfClass(LineJSON.getString(JSON_CLASS_START));
        ClassUnit end = dataManager.getNameOfClass(LineJSON.getString(JSON_CLASS_END));
        ClassUnit type = dataManager.getNameOfClass(LineJSON.getString(JSON_LINE_TYPE));
        LineUnit line = new LineUnit(start, end, type);

        return line;
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    public double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    /**
     * This method exports the contents of the data manager to a Web page
     * including the html page, needed directories, and the CSS file.
     *
     * @param data The data management component.
     *
     * @param filePath Path (including file name/extension) to where to export
     * the page to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        JsonObject json = saveDataForCode(data);
        JsonArray ClassArray = json.getJsonArray(JSON_CLASS_OBJECT);
        for (int i = 0; i < ClassArray.size(); i++) {
            JsonObject ClassJSON = ClassArray.getJsonObject(i);
            Boolean isExternal = ClassJSON.getBoolean(JSON_CLASS_EXTERNAL);
            if (!isExternal) {
                StringWriter sw = new StringWriter();
                exportToJava(sw, ClassJSON);

                String className = ClassJSON.getString(JSON_CLASS_NAME);
                String packageName = ClassJSON.getString(JSON_CLASS_PACKAGE);

                packageName = packageName.replaceAll("\\.", "/");
                filePath = filePath + "/" + packageName;
                File f = new File(filePath);
//                Path path = Paths.get(filePath);
//                if (!Files.exists(path)) {
                if (!f.exists()) {
                    f.mkdirs();
                }
                File target = new File(filePath + "/"+ className + ".java");

                String prettyPrinted = sw.toString();
                PrintWriter pw = new PrintWriter(target.getPath());
                pw.write(prettyPrinted);
                pw.close();
            }
        }
    }

    public void exportToJava(StringWriter sw, JsonObject ClassJSON) {

        //String className = jsonClassProperty.getString(JSON_CLASS_NAME);
        //CLASS PACKAGE NAME
        String packageName = ClassJSON.getString(JSON_CLASS_PACKAGE);
        if (!packageName.equals("")) {
            sw.append("package " + packageName + ";\n");
        }
        putClassToSW(sw, ClassJSON);

    }

    public void putClassToSW(StringWriter sw, JsonObject ClassJSON) {

        String className = ClassJSON.getString(JSON_CLASS_NAME);

        Boolean isInterface = ClassJSON.getBoolean(JSON_CLASS_INTERFACE);

        // public class CLASSNAME RELATIONSHIP PARENT{
        if (isInterface) {
            sw.append("public interface" + " " + className + " ");
            //LOOK THROUGH ALL PARENT FIND INHERITS PARENT CLASS
            //THEN CHECK THE TYPE, IF IT IS INTERFACE USING INPLEMENTS, OTHERWISE USING EXTENDS
            //class Sub extends Super A, B {}
            if (hasInterfaceParents(ClassJSON)) {
                sw.append("extends ");
                JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
                JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
                String subSw = "";
                for (int i = 0; i < parentListJSON.size(); i++) {
                    Boolean parentisInterface = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INTERFACE);
                    Boolean isInherits = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INHERITS);
                    if (isInherits && parentisInterface) {
                        subSw += parentListJSON.getJsonObject(i).getString(JSON_CLASS_NAME) + ",";
                    }
                }
                subSw.substring(0, subSw.length() - 2);
                sw.append(subSw);
            }

            sw.append("{\n");
        } else {
            //LOOK THROUGH ALL PARENT FIND INHERITS PARENT CLASS
            //THEN CHECK THE TYPE, IF IT IS INTERFACE USING INPLEMENTS, OTHERWISE USING EXTENDS
            //class Sub extends Super implements A, B {}
            sw.append("public class " + className + " ");
            if (hasParents(ClassJSON)) {
                sw.append("extends ");
                JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
                JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
                String subSw = "";
                for (int i = 0; i < parentListJSON.size(); i++) {
                    Boolean parentisInterface = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INTERFACE);
                    Boolean isInherits = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INHERITS);
                    if (isInherits && !parentisInterface) {
                        subSw += parentListJSON.getJsonObject(i).getString(JSON_CLASS_NAME) + ",";
                    }
                }
                if(subSw.length()>=2)
                    sw.append(subSw.substring(0, subSw.length()-2));
                else
                    sw.append(subSw);
            }

            if (hasInterfaceParents(ClassJSON)) {
                sw.append("implements ");
                JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
                JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
                String subSw = "";
                for (int i = 0; i < parentListJSON.size(); i++) {
                    Boolean parentisInterface = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INTERFACE);
                    Boolean isInherits = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INHERITS);
                    if (isInherits && parentisInterface) {
                        subSw += parentListJSON.getJsonObject(i).getString(JSON_CLASS_NAME) + ",";
                    }
                }
                if(subSw.length()>=2)
                    sw.append(subSw.substring(0, subSw.length()-2));
                else
                    sw.append(subSw);
            }

            sw.append("{\n");
        }

        JsonObject VarJSON = ClassJSON.getJsonObject(JSON_VAR);
        JsonArray varListJSON = VarJSON.getJsonArray(JSON_VAR);
        swFromVar(sw, varListJSON);
        JsonObject methodJSON = ClassJSON.getJsonObject(JSON_METHOD);
        JsonArray methodListJSON = methodJSON.getJsonArray(JSON_METHOD);
        putMethodToStringWriter(sw, methodListJSON);

        sw.append("}");

    }

    public Boolean hasParents(JsonObject ClassJSON) {
        JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
        JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
        for (int i = 0; i < parentListJSON.size(); i++) {
            Boolean isInterface = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INTERFACE);
            Boolean isInherits = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INHERITS);
            if (isInherits && !isInterface) {
                return true;
            }
        }
        return false;
    }

    public Boolean hasInterfaceParents(JsonObject ClassJSON) {
        JsonObject ParentJSON = ClassJSON.getJsonObject(JSON_CLASS_PARENT);
        JsonArray parentListJSON = ParentJSON.getJsonArray(JSON_CLASS_PARENT);
        for (int i = 0; i < parentListJSON.size(); i++) {
            Boolean isInterface = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INTERFACE);
            Boolean isInherits = parentListJSON.getJsonObject(i).getBoolean(JSON_CLASS_INHERITS);
            if (isInherits && isInterface) {
                return true;
            }
        }
        return false;
    }

    public void swFromVar(StringWriter sw, JsonArray varListJSON) {
        for (int i = 0; i < varListJSON.size(); i++) {
            String name = varListJSON.getJsonObject(i).getString(JSON_VAR_NAME);
            String type = varListJSON.getJsonObject(i).getString(JSON_VAR_TYPE);
            Boolean isStatic = varListJSON.getJsonObject(i).getBoolean(JSON_VAR_STATIC);
            String staticType = "";
            if(isStatic)
                staticType = "static";

            String Access = varListJSON.getJsonObject(i).getString(JSON_VAR_ACCESS);
            sw.append(JAVA_FOUR_SPACES + Access + " " + staticType + " " + type + " " + name + ";\n");
        }
    }

    public StringWriter putMethodToStringWriter(StringWriter sw, JsonArray methodListJSON) {
        for (int i = 0; i < methodListJSON.size(); i++) {
            String name = methodListJSON.getJsonObject(i).getString(JSON_METHOD_NAME);
            String type = methodListJSON.getJsonObject(i).getString(JSON_METHOD_RETURN_TYPE);
            Boolean isStatic = methodListJSON.getJsonObject(i).getBoolean(JSON_METHOD_STATIC);
            Boolean m_abstract = methodListJSON.getJsonObject(i).getBoolean(JSON_METHOD_ABSTRACT);
            String Access = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ACCESS);
            String m_arg_name = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ARG_NAME);
            String m_arg_type = methodListJSON.getJsonObject(i).getString(JSON_METHOD_ARG_TYPE);
            sw.append(JAVA_FOUR_SPACES + Access + " ");
            if (isStatic) {
                sw.append("static" + " ");
            }
            sw.append(type + " " + name + " ");
            String display = "(";
            String[] methodType_array = m_arg_type.split(",");
            String[] methodName_array = m_arg_name.split(",");
            display += methodType_array[0];
            display += " " + methodName_array[0];
            for (int j = 1; j < methodType_array.length; j++) {
                display += "," + methodType_array[j];
                display += " " + methodName_array[j];
            }
            display += " ){ ";
            if(!type.equals("void"))
                display +="return "+returnType(type)+";";
            display += " }\n";
            sw.append(display);
//            if(m_abstract)
//                sw.append("{abstract}");

        }
        return sw;
    }

    public String returnType(String type) {
        String[] typelist = {"byte", "int", "short", "long", "float", "double", "char"};
        for (String i : typelist) {
            if (i.equals(type)) {
                return "0";
            }
        }
        if (type.equals("boolean")) {
            return "true";
        } else {
            return "null";
        }

    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT THE Web Page Maker APPLICATION MAKES
        // NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
        // EXPORTED WEB PAGES
    }

    public JsonObject saveDataForCode(AppDataComponent data) {
        // GET THE DATA
        DataManager dataManager = (DataManager) data;

        ObservableList<Node> classUnitList = dataManager.getShapes();

        JsonArrayBuilder class_list = Json.createArrayBuilder();
        JsonArrayBuilder class_line_list = Json.createArrayBuilder();
        for (Node node : classUnitList) {
            // bu zhi dao zen me xie
            if (node instanceof ClassUnit) {
                ClassUnit classBox = (ClassUnit) node;
                class_list.add(makeClassUnit(classBox));
            } else if (node instanceof LineUnit) {
                LineUnit classLine = (LineUnit) node;
                class_line_list.add(makeLineJSON(classLine));
            }

        }
        JsonArray class_listJSON = class_list.build();
        JsonArray line_listJSON = class_line_list.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_CLASS_OBJECT, class_listJSON)
                .add(JSON_CLASS_LINE_OBJECT, line_listJSON)
                .build();
        return dataManagerJSO;

    }
}
