/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

/**
 *
 * @author xingye
 */
public class LineUnit extends Line {

    double startX, startY, endX, endY;
    ClassUnit start, end, connectorType;
    Polygon connector;

    public LineUnit(ClassUnit startUnit, double x, double y) {
        start = startUnit;
        end = new ClassUnit();
        connectorType = startUnit;
        connector = new Polygon();
        startX = startUnit.getLayoutX();
        startY = startUnit.getLayoutY();
        endX = x;
        endY = y;
        setStartX(startX);
        setStartY(startY);
        setEndX(endX);
        setEndY(endY);

    }

    public LineUnit(double x, double y, ClassUnit newEnd) {
        start = new ClassUnit();
        connectorType = newEnd;
        end = newEnd;
        if (newEnd.isAggregate) {
            endX = end.getLayoutX();
            endY = end.getLayoutY() + newEnd.height / 2;
        } else if (newEnd.getIsInherits()) {
            endX = end.getLayoutX() + newEnd.width / 2;
            endY = end.getLayoutY() + newEnd.height;
        } else if (newEnd.isUse) {
            endX = end.getLayoutX() + newEnd.width / 3;
            endY = end.getLayoutY() + newEnd.height;
        }
        connector = new Polygon();
        startX = x;
        startY = y;
        setStartX(startX);
        setStartY(startY);
        setEndX(endX);
        setEndY(endY);
    }
    
    public LineUnit(ClassUnit newStart, ClassUnit newEnd, ClassUnit type) {
        start = newStart;
        end = newEnd;
        connectorType = type;
        if (type.isAggregate) {
            endX = end.getLayoutX();
            endY = end.getLayoutY() + newEnd.height / 2;
        } else if (type.getIsInherits()) {
            endX = end.getLayoutX() + newEnd.width / 2;
            endY = end.getLayoutY() + newEnd.height;
        } else if (type.isUse) {
            endX = end.getLayoutX() + newEnd.width / 3;
            endY = end.getLayoutY() + newEnd.height;
        }
        connector = new Polygon();
        startX = start.getLayoutX();
        startY = start.getLayoutY();
        setStartX(startX);
        setStartY(startY);
        setEndX(endX);
        setEndY(endY);
    }
    
    

    public ClassUnit getConnectorType() {
        return connectorType;
    }
    
    public void setConnectorType(ClassUnit typeClass){
        connectorType = typeClass;
    }
    

    public void updateEnd() {
        setEndX(end.getLayoutX());
        setEndY(end.getLayoutY());
    }

    public void updateStart() {
        setStartX(start.getLayoutX());
        setStartY(start.getLayoutY());
    }

    public ClassUnit getStart() {
        return start;
    }

    public ClassUnit getEnd() {
        return end;
    }

    public double getAngle() {
        double angle = Math.toDegrees(Math.atan2(endY - startY, endX - startX));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    public void setEnd(ClassUnit newEnd) {
        end = newEnd;
        if (start.isAggregate) {
            endX = end.getLayoutX();
            endY = end.getLayoutY() + newEnd.height / 2;
        } else if (newEnd.isInherits) {
            endX = end.getLayoutX() + newEnd.width / 2;
            endY = end.getLayoutY() + newEnd.height;
        } else if (newEnd.isUse) {
            endX = end.getLayoutX() + newEnd.width / 3;
            endY = end.getLayoutY() + newEnd.height;
        }

        setEndX(endX);
        setEndY(endY);

        connector.getPoints().clear();
        if (connectorType.isAggregate) {
            connector.getPoints().addAll(new Double[]{endX + 12, endY, endX, endY - 8, endX - 12, endY, endX, endY + 8});
            connector.setRotate(getAngle());
        } else if (connectorType.getIsInherits()) {
            connector.getPoints().addAll(new Double[]{endX + 8, endY, endX - 7, endY + 7, endX - 7, endY - 7});
            connector.setRotate(getAngle());
        } else if (connectorType.isUse) {
            connector.getPoints().addAll(new Double[]{endX + 8, endY, endX - 7, endY + 7, endX - 7, endY - 7});
            connector.setFill(Color.WHEAT);
            connector.setRotate(getAngle());
        }

    }

    public void setStart(ClassUnit newStart) {
        start = newStart;
        startX = start.getLayoutX();
        startY = start.getLayoutY();
        setStartX(startX);
        setStartY(startY);
        connector.setRotate(getAngle());
    }

    public Boolean isStart(ClassUnit test) {
        return test.equals(start);
    }

    public Boolean isEnd(ClassUnit test) {
        return test.equals(end);
    }

    public Polygon addConnectorSign(ClassUnit target) {
        connectorType = target;
        if (target.isAggregate) {
            Polygon isAggregate = new Polygon(new double[]{endX + 12, endY, endX, endY - 8, endX - 12, endY, endX, endY + 8});

            connector = isAggregate;
            connector.setRotate(getAngle());
            return connector;
        } else if (target.getIsInherits()) {
            Polygon isInherits = new Polygon(new double[]{endX + 8, endY, endX - 7, endY + 7, endX - 7, endY - 7});
            connector = isInherits;
            connector.setRotate(getAngle());
            return connector;
        } else if (target.isUse) {
            Polygon isImplements = new Polygon(new double[]{endX + 8, endY, endX - 7, endY + 7, endX - 7, endY - 7});
            isImplements.setFill(Color.WHEAT);
            isImplements.setStroke(Color.BLACK);
            connector = isImplements;
            connector.setRotate(getAngle());
            return connector;
        } else {

            return connector;
        }
    }

}
