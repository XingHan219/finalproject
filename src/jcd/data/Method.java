/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;

/**
 *
 * @author xingye
 */
public class Method {
    
    private String name;
    private String returnType;
    private Boolean isStatic;
    private Boolean isAbstract;
    private String Access;
    private String argName;
    private String argType;
    private ArrayList<Var> varList;
    
    
    public Method(String name, String returnType, Boolean isStatic, Boolean isAbstract, String Access, ArrayList<Var> varList){
        
        this.name = name;
        this.returnType = returnType;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        this.Access = Access;
        this.varList = varList;
        
    }
    
    public Method(String name, String returnType, Boolean isStatic, Boolean isAbstract, String Access, String argName1, String argType1){
        
        this.name = name;
        this.returnType = returnType;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        this.Access = Access;
        this.argName = argName1;
        this.argType = argType1;

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the returnType
     */
    public String getReturnType() {
        return returnType;
    }

    /**
     * @param returnType the returnType to set
     */
    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    /**
     * @return the isStatic
     */
    public Boolean getIsStatic() {
        return isStatic;
    }

    /**
     * @param isStatic the isStatic to set
     */
    public void setIsStatic(Boolean isStatic) {
        this.isStatic = isStatic;
    }

    /**
     * @return the isAbstract
     */
    public Boolean getIsAbstract() {
        return isAbstract;
    }

    /**
     * @param isAbstract the isAbstract to set
     */
    public void setIsAbstract(Boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    /**
     * @return the Access
     */
    public String getAccess() {
        return Access;
    }

    /**
     * @param Access the Access to set
     */
    public void setAccess(String Access) {
        this.Access = Access;
    }

    /**
     * @return the argName
     */
    public String getArgName() {
        return argName;
    }

    /**
     * @param argName the argName to set
     */
    public void setArgName(String argName) {
        this.argName = argName;
    }

    /**
     * @return the argType
     */
    public String getArgType() {
        return argType;
    }

    /**
     * @param argType the argType to set
     */
    public void setArgType(String argType) {
        this.argType = argType;
    }

    /**
     * @return the varList
     */
    public ArrayList<Var> getVarList() {
        return varList;
    }

    /**
     * @param varList the varList to set
     */
    public void setVarList(ArrayList<Var> varList) {
        this.varList = varList;
    }
    
    
    
    
    
    
}
