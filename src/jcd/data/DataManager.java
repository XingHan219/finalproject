package jcd.data;

import java.util.Stack;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import static jcd.data.PoseMakerState.SELECTING_SHAPE;
import jcd.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {

    // THESE ARE THE SHAPES TO DRAW
    Stack<ObservableList<Node>> undoList;
    Stack<ObservableList<Node>> redoList;
    ObservableList<Node> shapes;
    // THE BACKGROUND COLOR
    Color backgroundColor;

    // AND NOW THE EDITING DATA
    // THIS IS THE SHAPE CURRENTLY BEING SIZED BUT NOT YET ADDED
    Node newClassUnit;

    // THIS IS THE SHAPE CURRENTLY SELECTED
    Node newSelectedClassUnit;

    // FOR FILL AND OUTLINE
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;
    double factor;

    // CURRENT STATE OF THE APP
    PoseMakerState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;

    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     * @throws java.lang.Exception
     */
    public DataManager(AppTemplate initApp) throws Exception {
        // KEEP THE APP FOR LATER
        app = initApp;
        undoList = new Stack();
        redoList = new Stack();
        // NO SHAPE STARTS OUT AS SELECTED
        newClassUnit = null;
        newSelectedClassUnit = null;

        // INIT THE COLORS
        currentFillColor = Color.web(WHITE_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);
        currentBorderWidth = 1;

        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(15);
        highlightedEffect = dropShadowEffect;
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        setState(SELECTING_SHAPE);
        newClassUnit = null;
        newSelectedClassUnit = null;
        shapes.clear();
        ((Workspace) app.getWorkspaceComponent()).getSnapshotPane().getChildren().clear();
    }

    public void undo(Node newSelectedClassUnit) {
        ObservableList<Node> undoShape = FXCollections.observableArrayList();
        //System.out.println(shapes.size() + "save size");
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) instanceof ClassUnit) {
                ClassUnit temp = (ClassUnit) shapes.get(i);
                undoShape.add(temp);
                //System.out.println("save NO." + i);
                
            }
        }
        undoList.push(undoShape);
    }
    
    public void redoChange(ObservableList<Node> replace){
        //undo(newSelectedClassUnit);
        shapes.clear();
        ObservableList<Node> redoShape = FXCollections.observableArrayList();
        //System.out.println(replace.size() + "load size");
        for (int i = 0; i < replace.size(); i++) {
            ClassUnit temp = (ClassUnit) replace.get(i);
            shapes.add(temp);
            redoShape.add(temp);
            //System.out.println("load No." + i);
        }
        //undoList.push(redoShape);
    }

    public void changeShapes(ObservableList<Node> replace) {
        //undo(newSelectedClassUnit);
        redoList.push(replace);
        shapes.clear();
        ObservableList<Node> redoShape = FXCollections.observableArrayList();
        //System.out.println(replace.size() + "load size");
        for (int i = 0; i < replace.size(); i++) {
            shapes.add(replace.get(i));
            redoShape.add(replace.get(i));
            //System.out.println("load No." + i);
        }
        

    }

    public void setMenuItem(Menu parentMenu) {
        parentMenu.getItems().clear();
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) instanceof ClassUnit) {
                ClassUnit classInDesign = (ClassUnit) shapes.get(i);

                CheckMenuItem newCheckItem = new CheckMenuItem(classInDesign.getClassName());
                parentMenu.getItems().add(newCheckItem);
                ClassUnit start = (ClassUnit) newSelectedClassUnit;
                ObservableList<ClassUnit> classParentList = start.getParentList();
                for (int j = 0; j < classParentList.size(); j++) {
                    System.out.println(classParentList.size());
                    if (classParentList.get(j).getClassName().equals(classInDesign.getClassName())) {
                        newCheckItem.setSelected(true);
                    }
                }
            }
        }
        // LAST MENU FOR ADD EXTERNAL CLASS
        MenuItem Add = new MenuItem("+ EXTERNAL CLASS");
        parentMenu.getItems().add(Add);

    }

    public void removeSelectedShape() {
        if (newSelectedClassUnit != null) {
            undo(newSelectedClassUnit);
            shapes.remove(newSelectedClassUnit);
            removeSelectedLine();
            removeSelectedParent();
            newSelectedClassUnit = null;
        }
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.loadSelectedShapeSettings(newSelectedClassUnit);
    }

    public void uncheckParentClass(String parent) {
        for (int i = 0; i < shapes.size(); i++) {
            Node temp_node = shapes.get(i);
            if (temp_node instanceof ClassUnit) {
                ClassUnit tempClass = (ClassUnit) temp_node;
                if (tempClass.getClassName().equals(parent)) {
                    removeSelectedLine(tempClass);
                }
            }
        }
    }

    public void removeSelectedLine(ClassUnit unchecked) {
        for (int i = 0; i < shapes.size(); i++) {
            Node temp_node = shapes.get(i);
            if (temp_node instanceof LineUnit) {
                LineUnit tempLine = (LineUnit) temp_node;
                unchecked.getLine().remove(tempLine);
                ClassUnit start = (ClassUnit) newSelectedClassUnit;
                start.getLine().remove(tempLine);
                System.out.println("remove check ");
                if (tempLine.isEnd(unchecked)) {
                    shapes.remove(temp_node);
                    System.out.println("remove line ");
                }
            }
        }
    }

    public void removeSelectedParent() {
        if (newSelectedClassUnit instanceof ClassUnit) {
            for (int i = 0; i < shapes.size(); i++) {
                Node temp_node = shapes.get(i);
                if (temp_node instanceof ClassUnit) {
                    ClassUnit temp = (ClassUnit) temp_node;
                    ObservableList<ClassUnit> list = temp.getParentList();

                    ClassUnit start = (ClassUnit) newSelectedClassUnit;
                    for (int j = 0; j < list.size(); j++) {
                        ClassUnit parent = list.get(j);
                        if (parent == start) {
                            list.remove(parent);
                        }
                    }
                }
            }
        }
    }

    public void removeSelectedLine() {
        for (int i = 0; i < shapes.size(); i++) {
            Node temp_node = shapes.get(i);
            if (temp_node instanceof LineUnit) {
                LineUnit tempLine = (LineUnit) temp_node;
                if (newSelectedClassUnit instanceof ClassUnit) {
                    ClassUnit start = (ClassUnit) newSelectedClassUnit;
                    if (tempLine.isStart(start)) {
                        shapes.remove(temp_node);
                    }
                }
            }
        }
    }

    public void unhighlightShape(Node shape) {
        newSelectedClassUnit.setEffect(null);
    }

    public void highlightShape(Node shape) {
        shape.setEffect(highlightedEffect);
    }

    public void startNewClass(int x, int y) {
        ClassUnit newUnit = new ClassUnit();
        newUnit.startNewClassUnit(x, y);
        newClassUnit = newUnit;
        initNewShape();
    }

    public void startNewInterface(int x, int y) {
        ClassUnit newUnit = new ClassUnit();
        newUnit.startNewClassUnit(x, y);
        newUnit.setInterface();
        newClassUnit = newUnit;
        initNewShape();
    }

    public void initNewShape() {
        // DESELECT THE SELECTED SHAPE IF THERE IS ONE
        if (newSelectedClassUnit != null) {
            unhighlightShape(newSelectedClassUnit);
            newSelectedClassUnit = null;
        }
        undo(newSelectedClassUnit);
        shapes.add(newClassUnit);
        newSelectedClassUnit = newClassUnit;
        highlightShape(newSelectedClassUnit);

        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.loadSelectedShapeSettings(newSelectedClassUnit);

    }

    public Boolean setClassTypeToPane(ClassUnit opps) {

        for (Node k : shapes) {
            if (k instanceof ClassUnit) {
                ClassUnit tempShape = (ClassUnit) k;
                if (opps.getClassName().equals(tempShape.getClassName())) {
                    return true;
                }
            }
        }
        return false;

    }

    public Boolean checkType(String type) {
        ClassUnit temp = (ClassUnit) newSelectedClassUnit;
        return temp.checkType(type);
    }

    public void makeClassForType(String type) {
        if (newSelectedClassUnit != null) {
            undo(newSelectedClassUnit);
            ClassUnit end1 = (ClassUnit) newSelectedClassUnit;
            ClassUnit start = end1.makeNewClassType(type, end1.getLayoutX(), end1.getLayoutY());
            start.setAggregate();
            start.setExternal();
            LineUnit newLine = end1.addLineStart(start);
            newLine.setEnd(end1);
            start.addLine(newLine);
            if (!setClassTypeToPane(start)) {
                shapes.add(start);
                shapes.add(newLine);
                shapes.add(newLine.addConnectorSign(start));
            }
        }
    }

    public void updateLine() {
        ClassUnit moved = (ClassUnit) newSelectedClassUnit;
        for (int i = 0; i < shapes.size(); i++) {
            Node temp_node = shapes.get(i);
            if (temp_node instanceof LineUnit) {
                LineUnit test_line = (LineUnit) temp_node;
                if (test_line.isStart(moved)) {
                    test_line.setStart(moved);
                } else if (test_line.isEnd(moved)) {
                    test_line.setEnd(moved);
                }
            }
        }

    }

    public void addParentClass(ClassUnit parent) {
        if (parent != null) {
            System.out.println("add parent");
            undo(newSelectedClassUnit);
            ClassUnit start = (ClassUnit) newSelectedClassUnit;
            ClassUnit end = start.makeParentClass(parent);
            end.setIsInherits(Boolean.TRUE);
            LineUnit newLine = start.addLineEnd(end);
            newLine.setStart(start);
            end.addLine(newLine);
            shapes.add(newLine);
            shapes.add(newLine.addConnectorSign(end));
        }
    }

    public void addExternalClass() {
        undo(newSelectedClassUnit);
        ClassUnit start = (ClassUnit) newSelectedClassUnit;
        ClassUnit end = start.makeExternalClass();
        end.setExternal();
        LineUnit newLine = start.addLineEnd(end);
        newLine.setStart(start);
        end.addLine(newLine);
        if (!setClassTypeToPane(end)) {
            shapes.add(end);
            shapes.add(newLine);
            shapes.add(newLine.addConnectorSign(end));
        }

    }

    public void addUseClass(String type) {
        if (newSelectedClassUnit != null) {
            undo(newSelectedClassUnit);
            ClassUnit start = (ClassUnit) newSelectedClassUnit;
            ClassUnit end = start.makeNewClassType(type, start.getLayoutX(), start.getLayoutY());
            end.setUseType();
            end.setExternal();
            LineUnit newLine = start.addLineStart(start);
            newLine.setEnd(end);
            end.addLine(newLine);
            if (!setClassTypeToPane(end)) {
                shapes.add(end);
                shapes.add(newLine);
                shapes.add(newLine.addConnectorSign(end));
            }
        }
    }

    /*------------------------------------------------------------------------*/
 /*------------------------------------------------------------------------*/
 /*------------------------------------------------------------------------*/
 /*------------------------------------------------------------------------*/

    public ClassUnit getNameOfClass(String name) {
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) instanceof ClassUnit) {
                ClassUnit classInDesign = (ClassUnit) shapes.get(i);
                if (classInDesign.getClassName().equals(name)) {
                    return classInDesign;
                }
            }
        }
        return null;
    }

    public Node getNewShape() {
        return newClassUnit;
    }

    public Node getSelectedShape() {
        return newSelectedClassUnit;
    }

    public ClassUnit getSelectedClass() {
        if (newSelectedClassUnit instanceof ClassUnit) {
            ClassUnit unit = (ClassUnit) newSelectedClassUnit;
            return unit;
        }
        return null;
    }

    public ObservableList<Var> getSelectedVarList() {
        ClassUnit unit = (ClassUnit) newSelectedClassUnit;
        return unit.getNewVarList();
    }

    public ObservableList<Method> getSelectedMethodList() {
        ClassUnit unit = (ClassUnit) newSelectedClassUnit;
        return unit.getNewMethodList();
    }

    public String getSelectedClassName() {
        if (newSelectedClassUnit instanceof ClassUnit) {
            ClassUnit temp = (ClassUnit) newSelectedClassUnit;
            return temp.getClassName();
        }
        return null;
    }

    public void setSelectedClassName(String name) {
        if (newSelectedClassUnit instanceof ClassUnit) {
            //undo(newSelectedClassUnit);
            ClassUnit temp = (ClassUnit) newSelectedClassUnit;
            temp.setClassName(name);
        }
    }

    public String getSelectedPackageName() {
        if (newSelectedClassUnit instanceof ClassUnit) {
            ClassUnit temp = (ClassUnit) newSelectedClassUnit;
            return temp.getPackageName();
        }
        return null;
    }

    public void setSelectedPackageName(String name) {
        if (newSelectedClassUnit instanceof ClassUnit) {
            //undo(newSelectedClassUnit);
            ClassUnit temp = (ClassUnit) newSelectedClassUnit;
            temp.setPackageName(name);
        }
    }

    public boolean checkNewClassName(String name) {
        int flag = 0;
        for (Node added : shapes) {
            if (added instanceof ClassUnit) {
                ClassUnit temp = (ClassUnit) added;
                if (temp.getClassName().equals(name)) {
                    flag = 1;
                }
            }
        }
        return flag == 1;
    }

    public boolean checkNewPacageName(String name) {
        int flag = 0;
        for (Node added : shapes) {
            if (added instanceof ClassUnit) {
                ClassUnit temp = (ClassUnit) added;
                if (temp.getPackageName().equals(name)) {
                    flag = 1;
                }
            }
        }
        return flag == 1;
    }

    public void setSelectedShape(Node initSelectedShape) {
        newSelectedClassUnit = initSelectedShape;
    }

    public Node selectTopShape(Node top, int x, int y) {
        Node shape = top;

        if (newSelectedClassUnit != null) {
            unhighlightShape(newSelectedClassUnit);
        }
        if (shape != null) {
            highlightShape(shape);
            newSelectedClassUnit = shape;
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.loadSelectedShapeSettings(shape);
        }
//	newSelectedClassUnit = shape;
        return shape;
    }

    public Node getTopShape(int x, int y) {
        for (int i = shapes.size() - 1; i >= 0; i--) {
            Node shape = (Node) shapes.get(i);
            if (shape.contains(x, y)) {
                return shape;
            }
        }
        return null;
    }

    public void addShape(Node shapeToAdd) {
        shapes.add(shapeToAdd);
    }

    public void removeShape(Node shapeToRemove) {
        shapes.remove(shapeToRemove);

    }

    public PoseMakerState getState() {
        return state;
    }

    public void setState(PoseMakerState initState) {
        state = initState;
    }

    public boolean isInState(PoseMakerState testState) {
        return state == testState;
    }

    public void setGridFactor(double factor) {
        this.factor = factor;
    }

    public double getGridFactor() {
        return factor;
    }

    public Stack<ObservableList<Node>> getUndoList() {
        return undoList;
    }
    
    public Stack<ObservableList<Node>> getRedoList() {
        return redoList;
    }

    public ObservableList<Node> getShapes() {
        return shapes;
    }

    public void setShapes(ObservableList<Node> initShapes) {
        shapes = initShapes;
    }

}
