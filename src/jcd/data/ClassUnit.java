/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author xingye
 */
public class ClassUnit extends VBox {

    //CSS
    static final String CLASS_UNIT = "unit_class";

    VBox classUnit;
    VBox classTitle;
    VBox classVariables;
    VBox classMethods;
    // 3 TYPES FOR RELATIONSHIP
    Boolean isAggregate;
    Boolean isInherits;
    Boolean isUse;
    // 2 TYPES 
    Boolean isInterface;
    Boolean isExternal;
    
    Text className;
    String classPackageName;

    ObservableList<Var> varList;
    ObservableList<Method> methodList;
    ObservableList<ClassUnit> classParentList;
    ArrayList<LineUnit> classLine;
    ArrayList<String> parentNameList;

    double startX;
    double startY;
    double width;
    double height;

    public ClassUnit() {
        isAggregate = false;
        isInterface = false;
        isInherits = false;
        isUse = false;
        isExternal = false;
        
        startX = 0.0;
        startY = 0.0;
        width = 150;
        height = 100;
        classUnit = new VBox();
        classTitle = new VBox();

        className = new Text("NewClass");

        classPackageName = new String();
        classTitle.getChildren().add(className);
        getChildren().add(classTitle);

        varList = FXCollections.observableArrayList();
        classVariables = new VBox();
        getChildren().add(classVariables);
        classVariables.setStyle("-fx-border-style: solid ;");

        methodList = FXCollections.observableArrayList();
        classMethods = new VBox();
        getChildren().add(classMethods);
        classMethods.setStyle("-fx-border-style: solid ;");

        classParentList = FXCollections.observableArrayList();
        classLine = new ArrayList<LineUnit>();
        parentNameList = new ArrayList<String>();
    }
    
    public void loadClassUnit(Boolean isAggregate, Boolean isInterface, Boolean isInherits, Boolean isUse, Boolean isExternal,
            double x, double y, double newwidth, double newheight,String newclassName, String packageName){
        className.setText(newclassName);
        classPackageName = packageName;
        this.isAggregate = isAggregate;
        this.isInterface = isInterface;
        this.isInherits = isInherits;
        this.isUse = isUse;
        this.isExternal = isExternal;
        startX = x;
        startY = y;
        width = newwidth;
        height = newheight;
        classTitle.setPrefHeight(height / 3);
        setLayoutX(startX);
        setLayoutY(startY);
        setPrefHeight(height);
        setPrefWidth(width);
        changeNameToInterface();
        
        
        
    }
    
    public void addParentNameToList(String name){
        parentNameList.add(name);
    }
    
    public ArrayList<String> getParentNameList(){
        return parentNameList;
    }
    
    public void changeNameToInterface() {
        if (isInterface) {
            className.setText("<<" + className.getText() + ">>");
        } else {
            className.setText(className.getText());
        }
    }

    public void setParent(ObservableList<ClassUnit> parent) {
        classParentList = parent;
    }

    public ObservableList<ClassUnit> getParentList() {
        return classParentList;
    }
    
    public void addParentList(ClassUnit t) {
        classParentList.add(t);
    }


    public Boolean isAggregate() {
        return isAggregate;
    }

    public void setAggregate() {
        isAggregate = true;
    }

    public void addLine(LineUnit line) {
        classLine.add(line);

    }
    
    public ArrayList<LineUnit> getLine(){
        return classLine;
    }

    public LineUnit addLineStart(ClassUnit startUnit) {
        LineUnit line = new LineUnit(startUnit, startX, startY);
        classLine.add(line);
        return line;
    }
    
    public LineUnit addLineEnd(ClassUnit endUnit) {
        LineUnit line = new LineUnit(startX, startY, endUnit);        
        classLine.add(line);
        return line;
    }
    
    

    public Boolean checkType(String type) {
        String[] typelist = {"byte", "int", "short", "long", "float", "double", "char", "String", "boolean","void"};
        for (String i : typelist) {
            if (i.equals(type)) {
                return true;
            }
        }
        return false;
    }

    public ClassUnit makeNewClassType(String type, double x, double y) {
        ClassUnit newClassType = new ClassUnit();
        newClassType.setNewClassTpye(type, x, y);
        classParentList.add(newClassType);

        return newClassType;
    }
    
    public void setUseType(){
        if(!isInterface)
            isUse = true;
    }
    
    public Boolean isUse(){
        return isUse;
    }
    
    
    
    public ObservableList<String> getParentName(){
        ObservableList<String> classParentList_Name = FXCollections.observableArrayList();;
        for(int i=0; i<classParentList.size(); i++){
           ClassUnit temp = classParentList.get(i);
           classParentList_Name.add(temp.getClassName());
        }
        return classParentList_Name;
    }
    
    public ClassUnit makeParentClass(ClassUnit parent) {
        classParentList.add(parent);
        return parent;
    }
       
    public void setParentClass(String type, double x, double y) {

        setClassName(type);
        startX = x+50;
        //CLASS PARENT AT TOP
        startY = y-100;
        setLayoutX(startX);
        setLayoutY(startY);

        height = height / 2;
        width = width / 2;
        setPrefHeight(height);
        setPrefWidth(width);

        classTitle.setLayoutX(startX - 20);
        classTitle.setLayoutY(startY);
        classTitle.setPrefHeight(height / 2);
        classTitle.setPrefWidth(width / 2);

        getStyleClass().add(CLASS_UNIT);

    }

    public ClassUnit makeExternalClass() {        
        ClassUnit external = new ClassUnit();
        external.setExternal();
        external.setUseType();
        external.setClassName("ExternalClass");
    
        external.startNewClassUnit(startX-100, startY+50);
        classParentList.add(external);
        return external;
    }  
    
    public void setExternalSign(){
        Text isExternal = new Text("External");
        classVariables.getChildren().add(isExternal);
    }

    public void setNewClassTpye(String type, double x, double y) {

        setClassName(type);
        //CLASS TYPE AT LEFT
        startX = x - 150;
        startY = y;
        setLayoutX(startX);
        setLayoutY(startY);

        height = height / 2;
        width = width / 2;
        setPrefHeight(height);
        setPrefWidth(width);
        classTitle.setPrefHeight(height / 3);
        classTitle.setStyle("-fx-border-width: 0.5 ;");
        classTitle.setStyle("-fx-border-style: solid ;");
        classTitle.setStyle("-fx-background-color: #ffff66 ;");
        
//        classTitle.setLayoutX(startX - 20);
//        classTitle.setLayoutY(startY);
//        classTitle.setPrefHeight(height / 2);
//        classTitle.setPrefWidth(width / 2);

        getStyleClass().add(CLASS_UNIT);

    }


    public void startNewClassUnit(double x, double y) {
        startX = x;
        startY = y;
        setLayoutX(startX);
        setLayoutY(startY);

        setPrefHeight(height);
        setPrefWidth(width);

        classTitle.setLayoutX(startX);
        classTitle.setLayoutY(startY);
        classTitle.setPrefHeight(height / 3);
        classTitle.setPrefWidth(width);

        className.setLayoutX(startX);
        className.setLayoutY(startY);

        getStyleClass().add(CLASS_UNIT);
        classTitle.setStyle("-fx-border-width: 0.5 ;");
        classTitle.setStyle("-fx-border-style: solid ;");
        classTitle.setStyle("-fx-background-color: #ffff66 ;");
        
        
    }
    
    
    public void resizeClassUnit(double x, double y) {
        width = x - startX;
        height = y - startY;
        setPrefHeight(height);
        setPrefWidth(width);
    }

    public void dragToMove(int x, int y) {
        double diffX = x - (getLayoutX() + (getWidth() / 2));
        double diffY = y - (getLayoutY() + (getHeight() / 2));
        double newX = getLayoutX() + diffX;
        double newY = getLayoutY() + diffY;
        setLayoutX(newX);
        setLayoutY(newY);
        startX = newX;
        startY = newY;

    }

    public void drapSnap(int x, int y, double factory) {
        double diffX = x - (getLayoutX() + (getWidth() / 2));
        double diffY = y - (getLayoutY() + (getHeight() / 2));
        double newX = getLayoutX() + diffX;
        double newY = getLayoutY() + diffY;
        setLayoutX(newX - newX % factory);
        setLayoutY(newY - newY % factory);
        startX = newX - newX % factory;
        startY = newY - newY % factory;
    }

    

    public void addNewMethod() {
        Method defaultMethod = new Method("Name", "Type", false, false, "Access", "", "");
        methodList.add(defaultMethod);
    }
    
    public void addNewMethod(Method me) {
        
        methodList.add(me);
        setMethodDisplay();
    }

    public void setMethodDisplay() {
        classMethods.getChildren().clear();
        for (int i = 0; i < methodList.size(); i++) {
            String display = "";
            display += methodList.get(i).getName();
            display += "( ";
            String[] methodType_array = methodList.get(i).getArgType().split(","); 
            String[] methodName_array = methodList.get(i).getArgName().split(","); 
            display += methodType_array[0];
            display += " "+methodName_array[0];
            for(int j=1; j<methodType_array.length;j++){
                display += "," + methodType_array[j];
                display += " "+methodName_array[j];
            }
            display += " ) :";
            display += methodList.get(i).getReturnType();
            if (methodList.get(i).getIsStatic()) {
                display = "$" + display;
            }
            switch (methodList.get(i).getAccess()) {
                case "public":
                    display = "+" + display;
                    break;
                case "private":
                    display = "-" + display;
                    break;
                case "protected":
                    display = "#" + display;
                    break;
                default:
                    break;
            }
            
            if(methodList.get(i).getIsAbstract()){
                display += "{abstract}";
                
            }
                    
            Text methodText = new Text(display);
            classMethods.getChildren().add(methodText);

        }
    }

    public void setVarDisplay() {
        classVariables.getChildren().clear();
        for (int i = 0; i < varList.size(); i++) {
            String display = "";
            display += varList.get(i).getName() + ": ";
            display += varList.get(i).getType();
            if (varList.get(i).getIsStatic()) {
                display = "$" + display;
            }
            switch (varList.get(i).getAccess()) {
                case "public":
                    display = "+" + display;
                    break;
                case "private":
                    display = "-" + display;
                    break;
                case "protected":
                    display = "#" + display;
                    break;
                default:
                    break;
            }

            Text varText = new Text(display);
            classVariables.getChildren().add(varText);

        }
    }

    public void addNewVar() {
        Var defaultVar = new Var("Name", "Type", false, "Access");
        varList.add(defaultVar);
    }

    public void setClassName(String name) {
        if (isInterface) {
            className.setText("<<" + name + ">>");
        } else {
            className.setText(name);
        }
    }

    public void setInterface() {
        isInterface = true;
        className.setText("<<Interface>>");
    }
    
    public Boolean isInterface(){
        return isInterface;
    }

    public VBox getClassVBox() {
        return classUnit;
    }

    public String getClassName() {
        String name = className.getText();
        if (isInterface) {
            return name.substring(2, className.getText().length() - 2);
        } else {
            return name;
        }
    }

    public void setX(double x) {
        startX = x;
        setLayoutX(startX);
    }

    public double getX() {
        return startX;
    }

    public void setY(double y) {
        startY = y;
        setLayoutX(startY);
    }

    public double getY() {
        return startY;
    }

    public void setWidthOfUnit(double newWidth) {
        width = newWidth;
        setPrefWidth(width);
    }

    public double getWidthOfUnit() {
        return width;
    }

    public void setHeightOfUnit(double newHeight) {
        height = newHeight;
        setPrefHeight(height);
    }

    public double getHeightOfUnit() {
        return height;
    }

    public void setPackageName(String name) {
        classPackageName = name;
    }

    public String getPackageName() {
        return classPackageName;
    }
    
    public void setExternal(){
        isExternal = true;
    }
    
    public Boolean isExternal(){
        return isExternal;
    }
    
    public ObservableList<Var> getNewVarList() {
        return varList;
    }

    public void setNewVarList(ObservableList<Var> newVarList) {
        varList = newVarList;
        setVarDisplay();
    }
    
    public void addNewVar(Var var){
        varList.add(var);   
        setVarDisplay();
    }
    
    public ObservableList<Method> getNewMethodList() {
        return methodList;
    }

    public void setNewMethodList(ObservableList<Method> newMethodList) {
        methodList = newMethodList;
        setMethodDisplay();
    }

    /**
     * @return the isInherits
     */
    public Boolean getIsInherits() {
        return isInherits;
    }

    /**
     * @param isInherits the isInherits to set
     */
    public void setIsInherits(Boolean isInherits) {
        this.isInherits = isInherits;
    }
}
