package jcd.data;

/**
 * This enum has the various possible states of the pose maker app.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public enum PoseMakerState {
    ADDING_POINT,
    SELECTING_SHAPE,
    DRAGGING_SHAPE,
    SNAPPING_SHAPE,
    STARTING_CLASS,
    STARTING_INTERFACE,
    SIZING_SHAPE,
    DRAGGING_NOTHING,
    SIZING_NOTHING
}
