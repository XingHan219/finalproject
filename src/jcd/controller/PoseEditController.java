package jcd.controller;

import javafx.collections.ObservableList;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import jcd.data.ClassUnit;
import jcd.data.DataManager;
import jcd.data.Method;
import jcd.data.PoseMakerState;
import static jcd.data.PoseMakerState.SNAPPING_SHAPE;
import jcd.data.Var;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 * This class responds to interactions with other UI pose editing controls.
 *
 * @author Xing Han
 * @version 1.0
 */
public class PoseEditController {

    AppTemplate app;

    DataManager dataManager;

    public PoseEditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
    }
    
    public void getParentFromBox(Object node){
        Node temp = (Node) node;
        dataManager.selectTopShape(temp,0,0);
    }

    public void addClass() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(PoseMakerState.STARTING_CLASS);

        // ENABLE/DISABLE THE PROPER BUTTONS
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //workspace.reloadWorkspace();
    }
    
    public void addInterface(){
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(PoseMakerState.STARTING_INTERFACE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //workspace.reloadWorkspace();
        
    }
    
        
    public void removeParent(ClassUnit parent){
        //dataManager.removeParentClass(parent);
    }
    
    public void addVar() {
        if(dataManager.getSelectedClass()!=null){
            dataManager.getSelectedClass().addNewVar();
            dataManager.getSelectedClass().setVarDisplay();
        }
        
    }

    public void removeVar(ObservableList<Var> selectedVar) {
        if(dataManager.getSelectedClass()!=null){
            selectedVar.forEach(dataManager.getSelectedVarList()::remove);
            dataManager.getSelectedClass().setVarDisplay();
        }

    }
    
    public void updateVarTable(ObservableList<Var> newVarList){
        
        dataManager.getSelectedClass().setNewVarList(newVarList);
    }
    
    public void addMethod() {
        if(dataManager.getSelectedClass()!=null){
            dataManager.getSelectedClass().addNewMethod();
            dataManager.getSelectedClass().setMethodDisplay();
        }
    }

    public void removeMethod(ObservableList<Method> selectedMethod) {
        if(dataManager.getSelectedClass()!=null){
            selectedMethod.forEach(dataManager.getSelectedMethodList()::remove);
            dataManager.getSelectedClass().setMethodDisplay();
        }
    }
    
    public void updateMethodTable(ObservableList<Method> newMethodList){
        dataManager.getSelectedClass().setNewMethodList(newMethodList);
    }
    
    
    
    
    
    public void checkType(String type){
        if(!dataManager.checkType(type))
            dataManager.makeClassForType(type);
        
    }
    
    public void checkTypeForMethod(String type){
        String[] temp = type.split(",");
        for(int i=0; i<temp.length;i++){
            if(!dataManager.checkType(temp[i]))
                dataManager.addUseClass(temp[i]);
        }
    }
        
    public void setVarName(Var selectedVar, String name){
        selectedVar.setName(name);
        
    }

    public void processSelectSelectionTool() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);

        // CHANGE THE STATE
        if(!dataManager.isInState(SNAPPING_SHAPE))
            dataManager.setState(PoseMakerState.SELECTING_SHAPE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }
    
    public void SelectedToResize(){
        //CHANGE THE STATE
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);
        dataManager.setState(PoseMakerState.SIZING_SHAPE);
        
    }
    
    public void Snapping(double gridFactor){
        //CHANGE THE STATE
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.MOVE);
        dataManager.setState(PoseMakerState.SNAPPING_SHAPE);
        dataManager.setGridFactor(gridFactor);
    }
    
    public void SnappingOff(){
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);
        dataManager.setState(PoseMakerState.SELECTING_SHAPE);
    }
    
    
    public void addLineSegment(){
        dataManager.setState(PoseMakerState.ADDING_POINT);
    }
    
    public void processRemoveSelectedShape() {
	// REMOVE THE SELECTED SHAPE IF THERE IS ONE
	dataManager.removeSelectedShape();
	
	// ENABLE/DISABLE THE PROPER BUTTONS
//	Workspace workspace = (Workspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace();
//	app.getGUI().updateToolbarControls(false);
    }
//    public void SelectedToResize() {
//	//dataManager.moveSelectedShapeToBack();
//	app.getGUI().updateToolbarControls(false);
//    }
}
