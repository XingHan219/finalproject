package jcd.controller;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import jcd.data.ClassUnit;
import jcd.data.DataManager;
import jcd.data.LineUnit;
import jcd.data.PoseMakerState;
import static jcd.data.PoseMakerState.ADDING_POINT;
import static jcd.data.PoseMakerState.DRAGGING_NOTHING;
import static jcd.data.PoseMakerState.DRAGGING_SHAPE;
import static jcd.data.PoseMakerState.SELECTING_SHAPE;
import static jcd.data.PoseMakerState.SIZING_SHAPE;
import static jcd.data.PoseMakerState.SNAPPING_SHAPE;
import static jcd.data.PoseMakerState.STARTING_CLASS;
import static jcd.data.PoseMakerState.STARTING_INTERFACE;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author McKillaGorilla
 * @version 1.0
 */
public class CanvasController {

    AppTemplate app;

    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    public void processCanvasMouseClicked(int x, int y) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {

        } else if (dataManager.isInState(SIZING_SHAPE)) {

        } else if (dataManager.isInState(SNAPPING_SHAPE)) {

        } else if (dataManager.isInState(STARTING_CLASS)) {
            dataManager.startNewClass(x, y);
            
        } else if (dataManager.isInState(STARTING_INTERFACE)) {
            dataManager.startNewInterface(x, y);
            
        } else if(dataManager.isInState(ADDING_POINT)){
            
        }
    }
    

    public void processCanvasMouseExited(int x, int y) {
//	DataManager dataManager = (DataManager)app.getDataComponent();
//	if (dataManager.isInState(PoseMakerState.DRAGGING_SHAPE)) {
//	    
//	}
//	else if (dataManager.isInState(PoseMakerState.SIZING_SHAPE)) {
//	    
//	}
    }

    public void processCanvasMousePress(Node top, int x, int y) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Node shape = dataManager.selectTopShape(top, x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
            if (shape != null) {
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(PoseMakerState.DRAGGING_SHAPE);
                app.getWorkspaceComponent();
                app.getGUI().updateToolbarControls(false);
                if(dataManager.getSelectedClass()!=null){
                    //dataManager.undo(dataManager.getSelectedClass());
                }
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace();
            }
        }
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }

    public void processCanvasMouseMoved(int x, int y) {

        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            
            ClassUnit selected = (ClassUnit) dataManager.getSelectedShape();
            if (selected != null) {
                double startX = selected.getLayoutX();
                double startY = selected.getLayoutY();
                double height = selected.getHeight();
                double width = selected.getWidth();
                if (startX + width - 10 < x && x < startX + width && y < startY + height && y > startY + height - 10) {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.SE_RESIZE);
                }
            }

        }
    }

    public void processCanvasMouseDragged(int x, int y) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
//	    Draggable newDraggableShape = (Draggable)dataManager.getNewShape();
//	    newDraggableShape.size(x, y);
            ClassUnit selected = (ClassUnit) dataManager.getSelectedShape();
            selected.resizeClassUnit(x, y);
        } else if (dataManager.isInState(DRAGGING_SHAPE)) {
            ClassUnit selected = (ClassUnit) dataManager.getSelectedShape();
            selected.dragToMove(x, y);
            dataManager.updateLine();
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(SNAPPING_SHAPE)) {
            ClassUnit selected = (ClassUnit) dataManager.getSelectedShape();
            selected.drapSnap(x, y, dataManager.getGridFactor());
            dataManager.updateLine();
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processCanvasMouseRelease(int x, int y) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
//	    dataManager.selectSizedShape();
//	    app.getGUI().updateToolbarControls(false);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
        } else if (dataManager.isInState(PoseMakerState.DRAGGING_SHAPE)) {
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(PoseMakerState.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_SHAPE);
        }
    }
}
